package com.rhythm.rpg;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.rhythm.rpg.assets.Assets;
import com.rhythm.rpg.states.GameState;
import com.rhythm.rpg.states.MenuState;
import com.rhythm.rpg.states.State;

public class RhytmPG extends ApplicationAdapter {
	private Viewport viewport;
	public static OrthographicCamera camera;
	public static State gameState;
	public static State menuState;

	public final float WORLD_WIDTH = 160;
	public final float WORLD_HEIGHT = 90;

	@Override
	public void create() {
		Assets.init();
		initializeComponents();
	}

	@Override
	public void render() {
		camera.update();
		State.getState().update(Gdx.graphics.getDeltaTime(), camera);

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		State.getState().render(camera);

	}

	private void initializeComponents() {
		camera = new OrthographicCamera();
		viewport = new StretchViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
		viewport.apply();
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
		camera.update();
		gameState = new GameState();
		menuState = new MenuState();
		State.setState(menuState);
	}

	@Override
	public void dispose() {
		Assets.dispose();
		gameState.dispose();
		menuState.dispose();
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
	}

}
