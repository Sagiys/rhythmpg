package com.rhythm.rpg.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.rhythm.rpg.entities.Song;

public class Assets {

	//ui
	public static Texture playertest1;
	public static Texture playertest2;
	
	public static Texture tile;
	public static Texture tileDisabled;
	public static Texture board;
	public static Texture bumper;
	public static Texture bumperPressed;
	public static Texture trailmiddle;
	public static Texture trailside;
	public static FileHandle fontFileTest = Gdx.files.internal("fonts/LiberationMonoRegular.ttf");
	public static Song firstSong;
	
	public static void init() {
		//ui
		playertest1 = new Texture("ui/playertest1.png");
		playertest2 = new Texture("ui/playertest2.png");
		
		firstSong = new Song("songs/first.song");
		tile = new Texture("tile.png");
		tileDisabled = new Texture("tileDisabled.png");
		bumper = new Texture("bumper.png");
		bumperPressed = new Texture("bumperPressed.png");
		trailmiddle = new Texture("trailmiddle.png");
		trailside = new Texture("trailside.png");
		board = new Texture("board.png");
	}
	
	public static void dispose() {
		//ui
		playertest1.dispose();
		playertest2.dispose();
		
		tile.dispose();
		tileDisabled.dispose();
		bumper.dispose();
		bumperPressed.dispose();
		trailmiddle.dispose();
		trailside.dispose();
		board.dispose();
	}
	
	public static BitmapFont generateFont(FileHandle fontFile, int size, Color color) {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(fontFile);
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		parameter.color = color;
		BitmapFont font = generator.generateFont(parameter);
		generator.dispose();
		return font;
	}
	
}
