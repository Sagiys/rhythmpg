package com.rhythm.rpg.states;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.assets.Assets;
import com.rhythm.rpg.entities.Board;

public class GameState extends State {

	private Board board;
	
	public GameState() {
		batch = new SpriteBatch();
	}

	public void update(float delta, OrthographicCamera camera) {
		if (board != null)
			board.update(delta, camera);

		batch.setProjectionMatrix(camera.combined);

	}

	public void render(OrthographicCamera camera) {
		if (board == null)
			board = new Board(new Vector3(43f, 90f, 0f), new Vector3(76f, 90f, 0f), Assets.board, new int[] { Keys.D, Keys.F, Keys.J, Keys.K }, Assets.firstSong);

		batch.begin();
		if (board != null)
			board.render(batch, camera);
		batch.end();

	}

	public void dispose() {
		batch.dispose();
		if(board != null) board.dispose();
	}
}
