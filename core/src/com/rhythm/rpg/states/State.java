package com.rhythm.rpg.states;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class State {

	private static State currentState = null;
	protected SpriteBatch batch;

	public static void setState(State state) {
		currentState = state;
	}

	public static State getState() {
		return currentState;
	}

	public abstract void update(float delta, OrthographicCamera camera);

	public abstract void render(OrthographicCamera camera);

	public abstract void dispose();

}