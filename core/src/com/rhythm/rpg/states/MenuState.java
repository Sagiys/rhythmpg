package com.rhythm.rpg.states;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.RhytmPG;
import com.rhythm.rpg.assets.Assets;
import com.rhythm.rpg.ui.UIImageButton;
import com.rhythm.rpg.ui.UIManager;
import com.rhythm.rpg.ui.UISlider;
import com.rhythm.rpg.ui.UITextButton;

public class MenuState extends State{

	private UIManager uiManager;
	private UISlider horizontal, vertical;
	
	public MenuState() {
		batch = new SpriteBatch();
		uiManager = new UIManager();
		uiManager.addObject(new UITextButton(new Vector3(40f, 20f,0), new Vector3(35f, 5f, 0), "laba nie badz smutny", Assets.generateFont(Assets.fontFileTest, 20, Color.RED), Color.BLUE, () -> State.setState(RhytmPG.gameState)));
		uiManager.addObject(new UIImageButton(new Vector3(40f, 40f, 0), new Vector3(2f,2f,0), new Sprite(Assets.playertest1), new Sprite(Assets.playertest2), () -> System.out.println("dupsko")));
		horizontal = new UISlider(new Vector3(40f, 60f, 0), new Vector3(50f, 1f, 0), Color.BLUE, Color.PINK);
		vertical = new UISlider(new Vector3(5f, 10f, 0), new Vector3(5f, 30f, 0), Color.BLUE, Color.PINK);
		uiManager.addObject(horizontal);
		uiManager.addObject(vertical);
	}

	public void update(float delta, OrthographicCamera camera) {
		uiManager.update(delta, camera);
		vertical.setPosition(horizontal.getAmount());
	}

	public void render(OrthographicCamera camera) {
		uiManager.renderShapes(batch, camera);
		batch.begin();
		uiManager.render(batch, camera);
		batch.end();
	}

	public void dispose() {
		batch.dispose();
	}
	
}
