package com.rhythm.rpg.utils;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.RhytmPG;

public class CoordinatesUtils {

	public static Vector3 screenToWorld(int screenX, int screenY) {
		return RhytmPG.camera.unproject(new Vector3(screenX, screenY, 0));
	}
	
	public static Vector3 worldToScreen(Vector3 worldCords, OrthographicCamera camera) {
		return camera.project(worldCords);
	}
	
	public static Vector3 worldToScreen(float worldX, float worldY) {
		return RhytmPG.camera.project(new Vector3(worldX, worldY, 0));
	}

}
