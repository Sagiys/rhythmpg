package com.rhythm.rpg.utils;

import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.entities.Bumper;
import com.rhythm.rpg.entities.Tile;

public class MathUtils {

	public static Vector3 middlePoint(Vector3 position, Vector3 size) {
		return new Vector3(position.x + size.x / 2, position.y + size.y / 2, 0);
	}

	public static float returnDist(Bumper bumper, Tile tile) {
		Vector3 bumpMid = middlePoint(bumper.getPosition(), bumper.getSize());
		Vector3 tileMid = middlePoint(tile.getPosition(), tile.getSize());
		float dist = bumpMid.dst(tileMid);
		return dist;
	}
	
	public static float map(float x, float in_min, float in_max, float out_min, float out_max)
	{
	  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

}