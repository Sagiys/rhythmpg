package com.rhythm.rpg.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.utils.CoordinatesUtils;
import com.rhythm.rpg.utils.MathUtils;
import com.rhythm.rpg.utils.MouseMoveProcessor;

public class UISlider extends UIComponent{

	private Color sliderColor;
	private Color buttonColor;
	private Color baseColor;
	private Vector3 buttonSize;
	private Vector3 buttonPosition;
	private float amount;
	private ShapeRenderer sr;
	private boolean isHorizontal;
	
	public UISlider(Vector3 position, Vector3 size, Color sliderColor, Color buttonColor) {
		super(position, size);
		this.sliderColor = sliderColor;
		this.buttonColor = buttonColor;
		this.baseColor = buttonColor;
		amount = 0f;
		isHorizontal = (size.x > size.y) ? true : false; 		
		
		if(isHorizontal) {
			buttonSize = CoordinatesUtils.worldToScreen(1.5f, size.y + 3f);
			buttonPosition = new Vector3(this.position.x, this.position.y - (buttonSize.y - (buttonSize.y + this.size.y) / 2), 0);
			
			bounds = new Rectangle(position.x, position.y  + (size.y / 2f) - (size.y + 3f) / 2, size.x, size.y + 3f);

		} else {
			buttonSize = CoordinatesUtils.worldToScreen(size.x + 1f, 3f);
			buttonPosition = new Vector3(this.position.x - (buttonSize.x - this.size.x) /2f, this.position.y + this.size.y - buttonSize.y, 0);

			// needed for calculations
			Vector3 worldButtonSize = CoordinatesUtils.screenToWorld((int) buttonSize.x, (int) buttonSize.y);
			Vector3 worldButtonPosition = CoordinatesUtils.screenToWorld((int)buttonPosition.x, (int)buttonPosition.y);
			
			bounds = new Rectangle(worldButtonPosition.x, position.y , worldButtonSize.x, size.y);
		}
		
		
		
		sr = new ShapeRenderer();
	}

	@Override
	public void update(float delta, OrthographicCamera camera) {
		if(hover == true) {
			buttonColor = Color.RED;
			if(Gdx.input.isTouched()) {
				if(isHorizontal) {
					buttonPosition.x = (Gdx.input.getX() - buttonSize.x / 2F) * 1280F / Gdx.graphics.getWidth();
					if(buttonPosition.x < position.x) {
						buttonPosition.x = position.x;
					} else if(buttonPosition.x > position.x + size.x - buttonSize.x) {
						buttonPosition.x = position.x + size.x - buttonSize.x;
					}									
				} else {
					buttonPosition.y = (Gdx.graphics.getHeight() - Gdx.input.getY() - buttonSize.y / 2F) * 720F / Gdx.graphics.getHeight();
					if(buttonPosition.y > position.y + size.y - buttonSize.y) {
						buttonPosition.y = position.y + size.y - buttonSize.y;
					} else if(buttonPosition.y < position.y) {
						buttonPosition.y = position.y;
					}
				}
			}
		} else {
			buttonColor = baseColor;
		}
		amount = isHorizontal ? (buttonPosition.x - position.x) / (size.x - buttonSize.x): Math.abs(((buttonPosition.y - position.y + size.y - buttonSize.y) / (position.y - position.y + size.y - buttonSize.y)) -2F);

		System.out.println(amount);
	}

	@Override
	public void render(SpriteBatch batch, OrthographicCamera camera) {}

	@Override
	public void renderShapes(SpriteBatch batch, OrthographicCamera camera) {
		sr.setProjectionMatrix(batch.getProjectionMatrix());
		sr.begin(ShapeType.Filled);
		sr.setColor(sliderColor);
		sr.rect(position.x, position.y, size.x, size.y);
		sr.setColor(buttonColor);
		sr.rect(buttonPosition.x, buttonPosition.y, buttonSize.x, buttonSize.y);
		sr.end();
	}

	@Override
	public void onClick() {
		
	}
	
	// amount must be more or equal 0 or less or equal 1
	// amount represents buttonPosition on slider, also changes amount itself
	public void setPosition(float amount) {
		if(isHorizontal) {
			buttonPosition.x = MathUtils.map(amount, 0.0f, 1.0f, position.x, position.x + size.x - buttonSize.x);
		} else {
			buttonPosition.y = MathUtils.map(amount, 0.0f, 1.0f, position.y + size.y - buttonSize.y, position.y);
		}
	}
	
	public float getAmount() {
		return amount;
	}

}
