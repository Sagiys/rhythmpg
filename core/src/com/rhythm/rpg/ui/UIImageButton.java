package com.rhythm.rpg.ui;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public class UIImageButton extends UIComponent{

	private Sprite image;
	private Sprite hoverImage;
	private Sprite currentSprite;
	private ClickListener clicker;
	
	public UIImageButton(Vector3 position, Vector3 size, Sprite image, Sprite hoverImage, ClickListener clicker) {
		super(position, size);
		this.image = image;
		this.hoverImage = hoverImage;
		this.clicker = clicker;
				
		image.setSize(this.size.x, this.size.y);
		image.setPosition(this.position.x, this.position.y);
		hoverImage.setSize(this.size.x, this.size.y);
		hoverImage.setPosition(this.position.x, this.position.y);
		
		currentSprite = image;
	}

	public void update(float delta, OrthographicCamera camera) {
		if(hover) {
			currentSprite = hoverImage;
		} else {
			currentSprite = image;
		}
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		currentSprite.draw(batch);
	}

	public void renderShapes(SpriteBatch batch, OrthographicCamera camera) {}

	public void onClick() {
		clicker.onClick();
	}

}
