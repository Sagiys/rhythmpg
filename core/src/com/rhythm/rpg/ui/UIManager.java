package com.rhythm.rpg.ui;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class UIManager {

	private ArrayList<UIComponent> objects;
	
	public UIManager() {
		objects = new ArrayList<UIComponent>();
	}
	
	public void update(float delta, OrthographicCamera camera) {
		for(UIComponent object : objects) {
			object.update(delta, camera);
			object.onMouseMove(Gdx.input, camera);
			object.onMouseRelease();
		}
	}
	
	public void render(SpriteBatch batch, OrthographicCamera camera) {
		for(UIComponent object : objects) {
			object.render(batch, camera);
		}
	}
	
	public void renderShapes(SpriteBatch batch, OrthographicCamera camera) {
		for(UIComponent object : objects) {
			object.renderShapes(batch, camera);
		}
	}
	
	public void addObject(UIComponent object) {
		objects.add(object);
	}
	
	public void removeObject(UIComponent object) {
		objects.remove(object);
	}

	public ArrayList<UIComponent> getObjects() {
		return objects;
	}
	
}
