package com.rhythm.rpg.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.RhytmPG;
import com.rhythm.rpg.utils.CoordinatesUtils;

public abstract class UIComponent {

	protected Vector3 position;
	protected Vector3 size;
	protected Rectangle bounds;
	protected boolean hover = false;
	protected UIManager uiManager;
	
	public UIComponent(Vector3 position, Vector3 size) {
		this.position = CoordinatesUtils.worldToScreen(position.x, position.y);
		this.size = CoordinatesUtils.worldToScreen(size.x, size.y);
		bounds = new Rectangle(position.x, position.y, size.x, size.y);
	}
	
	public abstract void update(float delta, OrthographicCamera camera);
	public abstract void render(SpriteBatch batch, OrthographicCamera camera);
	public abstract void renderShapes(SpriteBatch batch, OrthographicCamera camera);
	public abstract void onClick();
	
	public void onMouseMove(Input input, OrthographicCamera camera) {
		Vector3 cords = CoordinatesUtils.screenToWorld(input.getX(), input.getY());
		if(bounds.contains(cords.x, cords.y)) {
			hover = true;
		} else {
			hover = false;
		}
	}
	
	public void onMouseRelease() {
		if(hover == true && Gdx.input.justTouched()) {
			onClick();
		}
	}

	public Vector3 getPosition() {
		return position;
	}

	public Vector3 getSize() {
		return size;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public boolean isHover() {
		return hover;
	}
	
}
