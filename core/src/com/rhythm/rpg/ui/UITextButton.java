package com.rhythm.rpg.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;

public class UITextButton extends UIComponent{

	private String text;
	private ClickListener clicker;
	private BitmapFont font;
	private Color backgroundColor;
	private ShapeRenderer sr;
	
	public UITextButton(Vector3 position, Vector3 size, String text, BitmapFont font, Color backgroundColor, ClickListener clicker) {
		super(position, size);
		sr = new ShapeRenderer();
		sr.setColor(backgroundColor);
		this.text = text;
		this.clicker = clicker;
		this.font = font;
		this.backgroundColor = backgroundColor;
	}

	public void update(float delta, OrthographicCamera camera) {
		if(hover) {
			sr.setColor(new Color(backgroundColor.r, backgroundColor.g, backgroundColor.b, .5f));
		} else {
			sr.setColor(backgroundColor);
		}
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {	
			GlyphLayout gl = new GlyphLayout(font, text);
			font.draw(batch, gl, position.x + (size.x / 2 - gl.width / 2), position.y + (size.y / 2 + gl.height / 2));		
	}
	
	public void renderShapes(SpriteBatch batch, OrthographicCamera camera) {
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		sr.setProjectionMatrix(batch.getProjectionMatrix());
		sr.begin(ShapeType.Filled);
		sr.rect(position.x, position.y, size.x, size.y);
		sr.end();
		Gdx.gl.glDisable(GL20.GL_BLEND);		
	}

	public void onClick() {
		clicker.onClick();
		hover = false;
	}

	
}
