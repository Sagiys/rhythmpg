package com.rhythm.rpg.ui;

public interface ClickListener {

	public void onClick();
	
}
