package com.rhythm.rpg.mappers;

public class TrailTimeMap {

	private int trail;
	private long time;

	public TrailTimeMap(int trail, long time) {
		this.trail = trail;
		this.time = time;
	}

	public int getTrail() {
		return trail;
	}

	public void setTrail(int trail) {
		this.trail = trail;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

}
