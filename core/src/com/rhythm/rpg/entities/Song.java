package com.rhythm.rpg.entities;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.rhythm.rpg.mappers.TrailTimeMap;

public class Song {

	private FileHandle songFile;
	private float speed;
	private ArrayList<ArrayList<TrailTimeMap>> notes;
	
	public Song(String path) {
		notes = new ArrayList<ArrayList<TrailTimeMap>>();
		ArrayList<String> strings = new ArrayList<String>();
		songFile = Gdx.files.internal(path);
		String wholeFile = songFile.readString();
		String lineArray[] = wholeFile.split("\\r?\\n");
		for(String line : lineArray) strings.add(line);
		
		speed = Float.parseFloat(strings.get(0));
		strings.remove(0);
		
		
		
		ArrayList<String[]> rawNotesFromFile = new ArrayList<String[]>();
		for(String s : strings) {
			String[] tokens = s.split(" ");
			rawNotesFromFile.add(tokens);
		}
		
		long shift = calculateShift(speed);
		
		for(int i = 0; i < rawNotesFromFile.size();) {
			// missing for loop iterator
			int iterator = 1;
			// helper variable - hold current note, needed for comparation
			String[] k = rawNotesFromFile.get(i);
			//initialize space for same timeStamp notes;
			ArrayList<TrailTimeMap> mapper = new ArrayList<TrailTimeMap>();
			//add to current timeStamp, single note
			mapper.add(new TrailTimeMap(Integer.parseInt(k[0]), Long.parseLong(k[1]) - shift));
			// if file has more notes go to loop
			if(i + iterator < rawNotesFromFile.size()) {
				// get next raw note
				String[] next = rawNotesFromFile.get(i + iterator);				
				// searching for same timeStamp in next notes
				while(next[1].equals(k[1])) {
					// if current note's timeStamp is equals to the next one, add to notes holder
					mapper.add(new TrailTimeMap(Integer.parseInt(rawNotesFromFile.get(i + iterator)[0]), Long.parseLong(rawNotesFromFile.get(i + iterator)[1]) - shift));
					// increment iterator
					iterator++;
					// searching for next possible note, if exists back to start of the loop to make timeStamp comparision again
					if(i + iterator < rawNotesFromFile.size())
						// get next raw note
						next = rawNotesFromFile.get(i + iterator);
					// if its not possible to obtain next note, break the while loop
					else
						break;
					
				}
			}
			// add timeStamp container with all notes that appears in same time to main container
			notes.add(mapper);
			//inrements for loop
			i += iterator;
		}				
	}
	
	public long calculateShift(float speed) {
		float shift = 1000f;
		float shiftHelper = (speed * 1000f) / 1.25f ;
		float diff = shift - shiftHelper;
		shift += diff;
		return (long)shift;
	}

	public ArrayList<ArrayList<TrailTimeMap>> getNotes() {
		return notes;
	}
	
	public float getSpeed() {
		return speed;
	}
}
