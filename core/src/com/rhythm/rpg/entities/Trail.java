package com.rhythm.rpg.entities;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.assets.Assets;
import com.rhythm.rpg.utils.MathUtils;

public class Trail extends Entity {

	private Bumper bumper;
	private ArrayList<Tile> tiles;
	private Tile nearest;
	private int score = 0;

	public int key;

	public Trail(Vector3 position, Vector3 size, Texture texture, int key) {
		super(new Vector3(position.x, 90f - size.y, 0), new Vector3(size.x, 90, 0), texture);
		this.key = key;
		bumper = new Bumper(new Vector3(position.x, 12f, 0), new Vector3(size.x, 5f, 0), Assets.bumper, key);
		tiles = new ArrayList<Tile>();

	}

	public void update(float delta, OrthographicCamera camera) {
		bumper.update(delta, camera);
		for (Iterator<Tile> i = tiles.iterator(); i.hasNext();) {
			Tile item = i.next();
			if (nearest == null || nearest.isPressed()) {
				nearest = item;
			}
			item.update(delta, camera);
			if (item.getPosition().y < 3f) {
				i.remove();
				nearest = null;
			}

		}
		input(camera);
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		sprite.draw(batch);
		bumper.render(batch, camera);
		for (Tile t : tiles) {
			t.render(batch, camera);
		}
	}

	public void checkForScore(boolean active) {
		if (nearest != null && active == false) {
			float dist = MathUtils.returnDist(bumper, nearest);
			if (dist < 7f) {
				if (nearest.isPressed() == false) {
					nearest.setPressed(true);
					if (dist < 1.0f)
						score += 320;
					else if (dist < 1.5f)
						score += 300;
					else if (dist < 3.0f)
						score += 200;
					else if (dist < 4.5f)
						score += 100;
					else if (dist < 5.5f)
						score += 50;
					else
						score += 0;
				}
			}
		}
	}

	private void input(OrthographicCamera camera) {
		if (Gdx.input.isKeyJustPressed(key))
			checkForScore(bumper.isActive());
		if (Gdx.input.isKeyPressed(key)) {
			bumper.setActive(true);
		} else {
			bumper.setActive(false);
		}
	}

	public ArrayList<Tile> getTiles() {
		return tiles;
	}

	public Bumper getBumper() {
		return bumper;
	}

	public Tile getNearest() {
		return nearest;
	}

	public void setTiles(ArrayList<Tile> tiles) {
		this.tiles = tiles;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
}