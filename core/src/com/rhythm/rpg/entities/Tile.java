package com.rhythm.rpg.entities;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.assets.Assets;

public class Tile extends Entity{
	
	private boolean pressed;
	private float speed;
	
	public Tile(Trail trail, float speed) {
		super(new Vector3(trail.getPosition().x + .5f, 93f, 0), new Vector3(trail.getSize().x - 1f, 3f, 0), Assets.tile);
		this.speed = speed;
		pressed = false;
	}

	public void update(float delta, OrthographicCamera camera) {
		sprite.setPosition(position.x, position.y);
		if(pressed) {
			sprite.setTexture(Assets.tileDisabled);;
		}
		
		position.y -= speed;
		
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		sprite.draw(batch);
	}	

	public boolean isPressed() {
		return pressed;
	}

	public void setPressed(boolean pressed) {
		this.pressed = pressed;
	}

	
	
}
