package com.rhythm.rpg.entities;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.assets.Assets;
import com.rhythm.rpg.mappers.TrailTimeMap;
import com.rhythm.rpg.utils.CoordinatesUtils;

public class Board extends Entity{
	
	private Trail[] trails = new Trail[4];
	private Song song;
	private Deque<ArrayList<TrailTimeMap>> groupedNotes;
	private int score = 0;
	private long start, now;

	public Board(Vector3 position, Vector3 size, Texture texture, int[] keys, Song song) {
		super(new Vector3(position.x, 90f - size.y, 0), new Vector3(size.x, 90f, 0f), texture);
		
		float delim = (size.x - 4f) / 4f;
		trails[0] = new Trail(new Vector3(position.x + 2f + delim * 0f ,90f,0f), new Vector3(delim,90f,0), Assets.trailside, keys[0]);
		trails[1] = new Trail(new Vector3(position.x + 2f + delim * 1f ,90f,0f), new Vector3(delim,90f,0), Assets.trailmiddle, keys[1]);
		trails[2] = new Trail(new Vector3(position.x + 2f + delim * 2f ,90f,0f), new Vector3(delim,90f,0), Assets.trailmiddle, keys[2]);
		trails[3] = new Trail(new Vector3(position.x + 2f + delim * 3f ,90f,0f), new Vector3(delim,90f,0), Assets.trailside, keys[3]);
		trails[3].flipSprite();
		this.song = song;
		
		groupedNotes = new ArrayDeque<ArrayList<TrailTimeMap>>();
		groupedNotes.addAll(song.getNotes());
		
		start = System.currentTimeMillis();
	}

	public void update(float delta, OrthographicCamera camera) {
		now = System.currentTimeMillis();
		
		if(!groupedNotes.isEmpty() && now - start > groupedNotes.peek().get(0).getTime()) {
			for(TrailTimeMap ttm : groupedNotes.pop()) {
				trails[ttm.getTrail()].getTiles().add(new Tile(trails[ttm.getTrail()], song.getSpeed() + .0444f));
			}
		}
		
		for(int i = 0; i < trails.length; i++) {
			trails[i].update(delta, camera);
			score += trails[i].getScore();
			trails[i].setScore(0);
			//optimize this code
			for(int j = 0; j < 4; j++) {
				Vector3 clickCords = CoordinatesUtils.screenToWorld(Gdx.input.getX(j), Gdx.input.getY(j));
				if(Gdx.input.isTouched(j)) {
					if (clickCords.x > trails[i].getPosition().x && clickCords.x < trails[i].getPosition().x + trails[i].getSize().x) {
						trails[i].checkForScore(trails[i].getBumper().isActive());						
						trails[i].getBumper().setActive(true);
					}
				}
			}
		}
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		for (Trail t : trails) {
			t.render(batch, camera);
		}
		sprite.draw(batch);
	}
	

	public String getScore() {
		
		return String.valueOf(score);
	}

}
