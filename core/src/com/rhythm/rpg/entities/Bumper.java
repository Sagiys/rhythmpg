package com.rhythm.rpg.entities;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.rhythm.rpg.assets.Assets;

public class Bumper extends Entity {

	private int key;
	private boolean active = false;

	public Bumper(Vector3 position, Vector3 size, Texture texture, int key) {
		super(position, size, texture);
		this.key = key;
	}

	public void update(float delta, OrthographicCamera camera) {
		if (active) {
			sprite.setTexture(Assets.bumperPressed);
		} else {
			sprite.setTexture(Assets.bumper);
		}
	}

	public void render(SpriteBatch batch, OrthographicCamera camera) {
		sprite.draw(batch);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getKey() {
		return key;
	}

	public void setKey(int key) {
		this.key = key;
	}

}
