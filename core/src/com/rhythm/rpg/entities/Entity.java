package com.rhythm.rpg.entities;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public abstract class Entity {

	protected Vector3 position;
	protected Vector3 size;

	protected Rectangle boundingBox;

	protected ShapeRenderer shapeRenderer;
	protected Sprite sprite;

	public Entity(Vector3 position, Vector3 size, Texture texture) {
		this.position = position;
		this.size = size;

		shapeRenderer = new ShapeRenderer();
		sprite = new Sprite(texture);

		sprite.setPosition(position.x, position.y);
		sprite.setSize(size.x, size.y);

		boundingBox = new Rectangle(position.x, position.y, size.x, size.y);
	}

	public abstract void update(float delta, OrthographicCamera camera);

	public abstract void render(SpriteBatch batch, OrthographicCamera camera);

	public void dispose() {
		shapeRenderer.dispose();
		sprite.getTexture().dispose();
	}
	
	public void flipSprite() {
		sprite.flip(true, false);
	}

	public Rectangle getBoundingBox() {
		return boundingBox;
	}

	public float getX() {
		return position.x;
	}

	public float getY() {
		return position.y;
	}

	public float getWidth() {
		return size.x;
	}

	public float getHeight() {
		return size.y;
	}

	public Vector3 getPosition() {
		return position;
	}

	public void setPosition(Vector3 position) {
		this.position = position;
	}

	public Vector3 getSize() {
		return size;
	}

	public void setSize(Vector3 size) {
		this.size = size;
	}

}
